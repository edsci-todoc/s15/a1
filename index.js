console.log('Hello, World')
// Details
const details = {
    firstName: "Ryan",
    lastName: "Todoc",
    age: 18,
    hobbies : [
        "playing basketball", "biking", "eating"
    ] ,
    address: {
        housenumber: "Lot 2/4, Block 24",
        street: "Dec. Street",
        city: "Caloocan City",
        state: "Southern Tagalog",
    }
}
const address = Object.values(details.address);
console.log("My First Name is " + details.firstName);
console.log("My Last Name is " + details.lastName);
console.log(`Yes, I am ${details.firstName} ${details.lastName}.`);
console.log("I am " + details.age + " years old.");
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I live at " + address.join(", ") + ".");
